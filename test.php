<?php


class Helper {
    static function getIfSet(& $var) {
        if (isset($var)) {
            return $var;
        }
        return null;
    }
}
session_start();
if (!isset($_SESSION['results'])) {
    $_SESSION['results'] = [];
}


resultPage();

function isCorrect(){
    $valueX = false; $valueY = false; $valueR = false;
    $y = str_replace(',', '.', ($_REQUEST['Y']));
    $x = str_replace(',', '.', ($_REQUEST['X']));
    $r = str_replace(',', '.', ($_REQUEST['R']));
    if ($y <= 3 && $y >= -5 && is_numeric($y)) $valueY = true;
    if  ($x <= 5 && $x >= -3 && is_numeric($x)) $valueX = true;
    if ($r <= 3 && $r >= -3 && is_numeric($r)) $valueR = true;
    return ($valueR && $valueX && $valueY);
 }

function writeHtml($html){
    return '<html lang="en">
                <head>
                <meta charset="UTF-8">
                    <title>ASCHHHHH</title>
                </head>
                <body id="mainBody">
             <table>
                    <tr>
    <th style="font: fantasy; padding: 8px; background-image: url(uff.jpeg); text-align: center; border: 5px ridge maroon; width: 275px; height: 150px">Ivashchenko Iana Andreevna<br>Group P3201<br>Var 201009</th></tr>
             <th><button style="height: 40px; width: 280px; background-color: burlywood; border-color: maroon">
                <a href=test.html style="color: chocolate">PLEASE BACK</a>
                </button>
            </th>
    </table>'
             . $html .
             '<table style="position: absolute; left: 0; top: 0; z-index: -1; background-color: maroon; width: 100vw;">
        <tr>
            <th><img src="2.jpg"></th>
            <th><img src="6.jpeg"></th>
        </tr>
        <tr>
            <th><img src="10.jpg"></th>
            <th><img src="3.jpg"></th>
        </tr>
    </table>
    </body>
            </html>';
 }

function Error(){
    echo '<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>RESULT PAGE</title>
    </head>
    <body id="mainBody" style="background-image: url(shash.jpeg); background-repeat: no-repeat; background-color: bisque; background-position: center">
        <table align="center">
            <tr height=35px></tr>
            <tr>
                <th style="font: fantasy; padding: 8px; background-image: url(uff.jpeg); text-align: center; border: 5px ridge maroon; width: 275px; height: 150px">Ivashchenko Iana Andreevna<br>Group P3201<br>Var 201009</th>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr><th></th></tr>
            <tr height=35px></tr>
            <th><button style="height: 40px; width: 280px; background-color: burlywood; border-color: maroon">
                <a href=test.html style="color: chocolate">PLEASE BACK</a>
                </button>
            </th>
        </table>
        <table align="center">
            <tr><th><img src="228.jpg" width=300px></th>
                <th width=60%></th>
                <th><img src="full.jpg" width=300px></th>
            </tr>
        </table>
        <table style="margin-left: 15%">
            <tr><th><img src="ahm.jpg" width=300px></th></tr>
        </table>
    </body>
</html>';
}

function resultTable(){
    return '<pre>
               
<table align="center" width="60%" bgcolor="chocolate">
    <tr style="padding: 8px; background-color: maroon; text-align: center;">
    <th width="10%">X</th>
    <th width="10%">Y</th>
    <th width="10%">R</th>
    <th width="10%">Result</th>
    <th width="10%">Time(msec)</th>
    <th width="10%">Current Time</th>
</tr>'.
generateTR(10).'
</table>
             
            </pre>';
}

function result(){
    $x = $_GET["X"];
    $y = $_GET["Y"];
    $r = $_GET["R"];
    $x = str_replace(',', ".", $x);
    $y = str_replace(',', ".", $y);
    $r = str_replace(',', ".", $r);
    if ($x <= 0 && $x >= ((-$r)/2)  && $y <= ($x+$r/2)) return true;
    elseif ($x <= 0 && $x >= (-$r) && $y <= 0 && $y >= (-$r)) return true;
    elseif ((($x*$x + $y*$y) <= ($r*$r)) && $x >= 0 && $y <= 0)  return true;
    else return false;
}

function generateTR($limit){
    while (count($_SESSION["results"]) >= $limit) array_shift($_SESSION["results"]);
     $tr = "";
     foreach ($_SESSION["results"] as $result){
         $tr = $tr . '<tr>';
         foreach ($result as $key=>$value){
             $tr = $tr.'<th width="10%">'.$value.'</th>';
         }
         $tr = $tr."</tr>";
     }
     return $tr;
}

function resultPage(){
    $startTime = explode(' ', microtime());
    $isCorrect = isCorrect();
    if (!$isCorrect){
        Error();
    }
    else {
        $x = $_GET["X"];
        $y = $_GET["Y"];
        $r = $_GET["R"];
        if (result()) $result="Hit";
        else $result="Miss";
        $endTime = explode(' ', microtime());
        $sec = $endTime[1] - $startTime[1]; 
        $mSec = $endTime[0] - $startTime[0];
        if ($sec === 0) $Time=round(($mSec*1000),3);
        else $Time=round(($sec+$mSec)*1000,3);
        $x = str_replace('+', "", $x);
        $y = str_replace('+', "", $y);
        $r = str_replace('+', "", $r);
        $_SESSION["results"][] = array("X" => $x,"Y" => $y, "R" => $r, "Result" => $result, "Time" => $Time, "CurrentTime" => date("H:i:s", strtotime('+3 hour')));
        $table = resultTable();
        echo writeHtml($table);
    }
}
?>